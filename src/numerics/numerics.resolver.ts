import { Int, Resolver } from '@nestjs/graphql';
import { Args, Query, Mutation } from '@nestjs/graphql';
import { NumericsService } from './numerics.service';
// import { BadRequestException } from '@nestjs/common';
import { FiboDto } from './dto/fibonacci.dto';
import { PalindromoDto } from './dto/palindromo.dto';
import { OperationsDto } from './dto/operations.dto';
import { ImcDto } from './dto/imc.dto';

import { TempDto } from './dto/temperatura.dto';

import { MasaDto } from './dto/masa.dto';
import { LongitudDto } from './dto/longitud.dto';

import { almacenamientoDto } from './dto/almacenamiento.dto';

import { objectOperation } from './model/operationModel';
import { temperatura } from './model/temperatura.model';
import { masa } from './model/masa.model';
import { longitud } from './model/longitud.model';
import { VolumenDto } from './dto/volumen.dto';
import { volumen } from './model/volumen.model';
import { almacenamiento } from './model/almacenamiento.model';
@Resolver()
export class NumericsResolver {
  constructor(private numericService: NumericsService) {}

  @Query(() => [Int])
  async LoopsFibonacci(@Args('loop') loop: FiboDto) {
    try {
      const fibonacci = [];
      fibonacci[0] = 0;
      fibonacci[1] = 1;
      for (let i = 2; i < loop.loop; i++) {
        fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
      }
      console.log(fibonacci);
      return fibonacci;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => [Int])
  async serieCollatz(@Args('loop') loop: FiboDto) {
    try {
      const collatz = [];

      while (loop.loop > 1) {
        if (loop.loop % 2 === 0) {
          loop.loop = loop.loop / 2;
          collatz.push(loop.loop);
        } else {
          loop.loop = loop.loop * 3 + 1;
          collatz.push(loop.loop);
        }
      }
      console.log(collatz);
      return collatz;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => Int)
  async factorial(@Args('loop') loop: FiboDto) {
    try {
      let result = 1;
      for (let i = loop.loop; i > 0; i--) {
        result *= i;
      }
      console.log(result);
      return result;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => String)
  async getImc(@Args('loop') data: ImcDto) {
    try {
      const imc = data.kg / Math.pow(data.cm / 100, 2);
      return imc;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => String)
  async ValidateNumberFibonacci(@Args('loop') number: FiboDto) {
    try {
      const root5 = Math.sqrt(5);
      const phi = (1 + root5) / 2;
      const idx = Math.floor(
        Math.log(number.loop * root5) / Math.log(phi) + 0.5,
      );
      const u = Math.floor(Math.pow(phi, idx) / root5 + 0.5);

      return u == number.loop;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => String)
  async ValidateFactorial(@Args('loop') number: FiboDto) {
    try {
      let result = 1;
      for (let i = number.loop; i > 0; i--) {
        result *= i;
      }

      return !!result;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => String)
  async ValidatePrimo(@Args('loop') number: FiboDto) {
    try {
      if (number.loop <= 1) return false;
      for (let i = 2; i <= number.loop - 1; i++)
        if (number.loop % i == 0) return false;
      return true;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => String)
  async ValidatePalindromo(@Args('loop') data: PalindromoDto) {
    try {
      for (let i = 0; i < data.data.length; i++) {
        if (data.data[i] != data.data[data.data.length - i - 1]) {
          return false;
        }
      }
      return true;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => objectOperation)
  async getOperations(@Args('loop') data: OperationsDto) {
    try {
      const arrayNumeros = [];
      for (let i = 0; i < data.totalNums; i++) {
        arrayNumeros.push(Math.floor(Math.random() * (data.max - data.min)));
      }

      const numMin = Math.min(...arrayNumeros);
      const numMax = Math.max(...arrayNumeros);

      const output = Array.from(
        { length: numMax - numMin },
        (v, i) => i + numMin,
      );

      const total = arrayNumeros.reduce((a, b) => a + b, 0);

      const object = {
        rango: output.toString(),
        promedio: total / data.totalNums,
        suma: total,
        minimo: numMin,
        maximo: numMax,
      };
      console.log(object);

      return object;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => temperatura)
  async getTemperatura(@Args('loop') data: TempDto) {
    try {
      console.log(data.data);
      const kelvin = data.data + 273.15;
      const fahrenheit = data.data * 1.8 + 32;
      const object = {
        kelvin,
        fahrenheit,
      };
      console.log(object);

      return object;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => masa)
  async getMasa(@Args('loop') data: MasaDto) {
    try {
      const gramos = data.gramo / 1000;
      const libra = data.gramo * 0.0022;
      const onza = data.gramo * 0.035274;

      const object = {
        gramos,
        libra,
        onza,
      };
      console.log(object);
      return object;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => longitud)
  async getLongitud(@Args('loop') data: LongitudDto) {
    try {
      const metros = data.cm / 100;
      const kilometros = data.cm / 1000;
      const pulgadas = data.cm * 0.39;

      const pies = data.cm / 30.48;
      const yardas = data.cm * 91.43999;
      const millas = data.cm * 160934.4;

      const object = {
        metros,
        kilometros,
        pulgadas,
        pies,
        yardas,
        millas,
      };
      console.log(object);
      return object;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => volumen)
  async getVolumen(@Args('loop') data: VolumenDto) {
    try {
      const litros = data.ml / 1000;
      const galones = data.ml / 3785;

      const object = {
        litros,
        galones,
      };
      console.log(object);
      return object;
    } catch (error) {
      console.log(error);
    }
  }

  @Query(() => almacenamiento)
  async getAlmacenamiento(@Args('loop') data: almacenamientoDto) {
    try {
      const Bytes = data.mb / 0.000001;
      const Gigabytes = data.mb / 1024;
      const Terabytes = data.mb / 1048576;

      const object = {
        Bytes,
        Gigabytes,
        Terabytes,
      };
      console.log(object);
      return object;
    } catch (error) {
      console.log(error);
    }
  }
}
