import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class PalindromoDto {
  @Field()
  data: string;
}
