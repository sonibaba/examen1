import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class OperationsDto {
  @Field()
  min: number;

  @Field()
  max: number;

  @Field()
  totalNums: number;
}
