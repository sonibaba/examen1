import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ImcDto {
  @Field()
  kg: number;

  @Field()
  cm: number;
}
