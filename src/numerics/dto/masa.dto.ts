import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class MasaDto {
  @Field()
  gramo: number;
}
