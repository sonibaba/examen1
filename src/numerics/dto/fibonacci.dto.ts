import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class FiboDto {
  @Field()
  loop: number;
}
