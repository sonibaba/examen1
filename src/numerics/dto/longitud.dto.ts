import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class LongitudDto {
  @Field()
  cm: number;
}
