import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class TempDto {
  @Field()
  data: number;
}
