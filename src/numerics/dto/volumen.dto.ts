import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class VolumenDto {
  @Field()
  ml: number;
}
