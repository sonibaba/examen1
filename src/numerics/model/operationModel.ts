import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class objectOperation {
  @Field()
  rango: string;

  @Field()
  promedio: number;

  @Field()
  suma: number;

  @Field()
  minimo: number;

  @Field()
  maximo: number;
}
