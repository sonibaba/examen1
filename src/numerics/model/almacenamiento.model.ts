import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class almacenamiento {
  @Field()
  Bytes: number;
  @Field()
  Gigabytes: number;
  @Field()
  Terabytes: number;
}
