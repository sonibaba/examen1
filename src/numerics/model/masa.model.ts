import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class masa {
  @Field()
  gramos: number;

  @Field()
  libra: number;

  @Field()
  onza: number;
}
