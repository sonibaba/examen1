import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class temperatura {
  @Field()
  kelvin: number;

  @Field()
  fahrenheit: number;
}
