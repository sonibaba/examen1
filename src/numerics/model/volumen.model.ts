import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class volumen {
  @Field()
  litros: number;
  @Field()
  galones: number;
}
