import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class longitud {
  @Field()
  metros: number;
  @Field()
  kilometros: number;
  @Field()
  pulgadas: number;
  @Field()
  pies: number;
  @Field()
  yardas: number;
  @Field()
  millas: number;
}
