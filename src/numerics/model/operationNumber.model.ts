import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class OperationNumber {
  @Field()
  num: number;
}
