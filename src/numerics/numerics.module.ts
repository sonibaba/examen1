import { Module } from '@nestjs/common';
import { NumericsService } from './numerics.service';
import { NumericsResolver } from './numerics.resolver';

@Module({
  providers: [NumericsService, NumericsResolver],
})
export class NumericsModule {}
